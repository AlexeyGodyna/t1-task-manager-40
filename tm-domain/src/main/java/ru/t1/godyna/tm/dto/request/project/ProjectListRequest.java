package ru.t1.godyna.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.request.AbstractUserRequest;
import ru.t1.godyna.tm.enumerated.ProjectSort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private ProjectSort sort;

    public ProjectListRequest(
            @Nullable final String token,
            @Nullable final ProjectSort sort
    ) {
        super(token);
        this.sort = sort;
    }

}
